package com.example.demo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.List;
import java.util.Optional;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {

		SpringApplication.run(DemoApplication.class, args);
	}

	@Bean
	public CommandLineRunner init(StudentRepository repository){
		return args -> {
			repository.save(new Student("mario","one",1991));
			repository.save(new Student("nemuel","one",1991));
			repository.save(new Student("pedro","one",1991));
			repository.save(new Student("juan","one",1991));
			repository.save(new Student("jacobo","one",1991));
			repository.save(new Student("mateo","one",1991));
			repository.save(new Student("charlie","two",1992));
			repository.save(new Student("marcos","two",1992));
			repository.save(new Student("lucas","two",1992));
			repository.save(new Student("betzabe","two",1992));
			repository.save(new Student("joel","two",1992));
			repository.save(new Student("lidia","three",1993));
			repository.save(new Student("pepe","three",1993));
			repository.save(new Student("lineth","three",1993));
			repository.save(new Student("damaris","three",1993));
			repository.save(new Student("leticia","three",1993));
			repository.save(new Student("maribel","three",1993));
			repository.save(new Student("marta","three",1993));
		};
	}

}


@Data
@AllArgsConstructor
@Entity
class Student{
	@Id
	@GeneratedValue
	private Long id;
	private String name;
	private String course;
	private int admissionYear;

	public Student() {
	}

	public Student(String s, String c, int i) {

		this.name = s;
		this.course = c;
		this.admissionYear = i;
	}
}

interface StudentRepository extends JpaRepository<Student, Long> {



	//List<Student> findByName(String name);

//	@Query("select s from Student s where name like %?1%")
//	List<Student> findByName(String name);

	@Query("select s from Student s where name like %?1%")
	Page<Student> findByName(String name, Pageable pageable);
}


@RestController
class StudentController{
	private final StudentRepository repository;

	StudentController(StudentRepository sr) {
		this.repository = sr;
	}

//	@GetMapping("/students")
//	public List<Student> findAll(@RequestParam Optional<String> name){
//		//return repository.findAll();
//		return repository.findByName(name.orElse("_"));
//	}

//	@GetMapping("/students")
//	public Page<Student> findAll(@RequestParam Optional<String> name){
//		//return repository.findAll();
//		return repository.findByName(name.orElse("_"),new PageRequest(0,5));
//	}

	@GetMapping("/students")
	public Page<Student> findAll(@RequestParam Optional<String> name,
								 @RequestParam Optional<Integer> page,
								 @RequestParam Optional<String> sortBy){
		//return repository.findAll();
		return repository.findByName(name.orElse("_"),new PageRequest(page.orElse(0),5, Sort.Direction.ASC, sortBy.orElse("id")));
	}
}
